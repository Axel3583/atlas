import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import OurRegions from '../screens/OurRegions';
import RegionDetails from '../screens/RegionDetails';
import AccountScreen from '../screens/AccountScreen';
import Itineraire from '../screens/Itineraire';

export default function Root() {

  const Drawer = createDrawerNavigator();

    return (
        <Drawer.Navigator screenOptions={{
            drawerStyle: {
              backgroundColor: '#f0edf6'
            },
          }} initialRouteName='Regions' useLegacyImplementation>
            <Drawer.Screen name="Regions" component={OurRegions} />
            <Drawer.Screen name="Explore" component={RegionDetails} />
            {/* <Drawer.Screen name="Departement" component={OurRegions} /> */}
            <Drawer.Screen name="Mon Itineraire" component={Itineraire} />
            <Drawer.Screen name="Mon compte" component={AccountScreen} />
            <Drawer.Screen name="Déconnexion" component={RegionDetails} />
            {/* <Stack.Screen name="Settings" component={EmptyScreen} /> */}
        </Drawer.Navigator>
    );
}
