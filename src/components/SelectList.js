import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { SelectList } from 'react-native-dropdown-select-list'
import { FontAwesome } from '@expo/vector-icons';

export default function Selectlist() {

    const [selected, setSelected] = React.useState("");

    const data = [
        { key: 'Paris', value: 'Île-de-France', disabled: true },
        { key: 'Lyon', value: 'Auvergne-Rhône-Alpes' },
        { key: 'Dijon', value: 'Bourgogne-Franche-Comté' },
        { key: 'Rennes', value: 'Bretagne', disabled: true },
        { key: 'Orléans', value: 'Centre-Val de Loire' },
        { key: 'Strasbourg', value: 'Grand Est' },
        { key: 'Lille', value: 'Hauts-de-France' },
        { key: 'Rouen', value: 'Normandie' },
        { key: 'Bordeaux', value: 'Nouvelle-Aquitaine' },
        { key: 'Toulouse', value: 'Occitanie' },
    ];
    

       // Trouver le département correspondant à la région sélectionnée
       const selectedDepartment = data.find(item => item.value === selected);

       return (
           <View style={styles.container}>
               <View style={styles.searchBar}>
                   <TouchableOpacity>
                       <SelectList
                           placeholder='Departements...'
                           setSelected={(val) => setSelected(val)}
                           data={data}
                           save="value"
                           arrowicon={<FontAwesome name="chevron-down" size={12} color={'black'} />}
                       />
                   </TouchableOpacity>
                   {selectedDepartment && (
                       <Text style={styles.selectedText}>
                           {selectedDepartment.key}
                       </Text>
                   )}
               </View>
           </View>
       )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingTop: 15
    },
    selectedText: {
        justifyContent: 'center',
        textAlign: 'center',
        backgroundColor: '#f0f8ff',
        width: 100,
        fontWeight: 400,
        borderRadius: 10,
        fontFamily: "Poppins",
    }
})