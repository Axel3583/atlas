import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Dimensions } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

export default function SearchBar({ onSearch }) {

  const [search, setSearch] = useState('')
 
  const updateText = (text) => {
    setSearch(text)
    // onSearch(text);
    console.log(text)
  }

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <TextInput
          onChangeText={updateText}
          value={search}
          placeholder="Recherche..."
          style={styles.searchInput}
        />
         <MaterialIcons name="search" size={30} color="#f0edf6" style={{paddingRight: 5}} />
      </View>
      {/* <View style={styles.text}>
      <MaterialIcons name="search" size={30} color="#4682b4" />
      </View> */}
    </View>
  );
}

const { width, height } = Dimensions.get('window');
const searchContainerWidth = width * 1;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    zIndex: 1,
    paddingVertical: 7
  },
  searchContainer: {
    backgroundColor: 'white',
    borderRadius: 15,
    // width: 355,
    // marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    width: width - 15,
    // shadowOffset: { width: 2, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    // elevation: 2
  },
  searchInput: {
    fontFamily: "Poppins",
    flex: 1,
    marginLeft: 8,
    height: 50,
    fontSize: 16,
    color: '#808080',
  },
  text: {
    flex: 1,
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#f0edf6',
    paddingHorizontal: width * 0.03,
    paddingVertical: height * 0.015,
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2
  }
});
