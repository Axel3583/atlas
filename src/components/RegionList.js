import React, { useState } from "react";
import { Dimensions } from "react-native"
import { useNavigation } from '@react-navigation/native';
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator
} from 'react-native';
import Root from "./Root";

export default function RegionList() {


  const navigation = useNavigation();
  const [extraData, setExtraData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isScrolling, setIsScrolling] = useState(false);

  const regionsData = [
    {
      id: '1',
      regionName: 'Auvergne-Rhône-Alpes',
      cityName: 'Lyon',
      imageUri: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg',
      culturalPlaces: [
        { id: '1', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '2', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://www.lemaraismood.fr/wp-content/uploads/2019/11/Place_de_la_Bastille_1841-le-marais-mood-1080x675.jpg' },
        { id: '3', name: 'Vieux Lyon', cityImage: 'https://cdn-s-www.dna.fr/images/6608B15B-5842-4DD4-A4BB-1650CAAFEC68/NW_raw/a-strasbourg-le-montant-de-la-taxe-fonciere-sur-le-bati-a-augmente-de-8-9-illustration-adobe-stock-1663163431.jpg' },
        { id: '4', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://parisfutur.com/wp-content/uploads/2019/09/Nouvelle-Place-de-la-Bastille-10.jpg' },
        { id: '5', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '6', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://a.cdn-hotels.com/gdcs/production59/d1337/3794b7dc-0609-4883-9ad2-51368c2e2c21.jpg?impolicy=fcrop&w=800&h=533&q=medium' },
        { id: '7', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/06/52/4e/e4/cathedrale-notre-dame.jpg?w=1200&h=-1&s=1' },
        { id: '8', name: 'Vieux Lyon', cityImage: 'https://www.sous-les-paves.com/wp-content/uploads/2020/05/Bastille-VV.jpg' },
        { id: '9', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://www.nerienlouper.paris/wp-content/uploads/2017/03/palais-opera-garnier.jpg' },
        { id: '10', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://upload.wikimedia.org/wikipedia/commons/5/58/Op%C3%A9ra_Bastille.jpg' },
        { id: '11', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://a.cdn-hotels.com/gdcs/production196/d1458/e3079228-50e3-43ea-9f64-30e4685eaf62.jpg?impolicy=fcrop&w=800&h=533&q=medium' },
        { id: '12', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '13', name: 'Vieux Lyon', cityImage: 'https://upload.wikimedia.org/wikipedia/commons/5/58/Op%C3%A9ra_Bastille.jpg' },
        { id: '14', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/06/52/4e/e4/cathedrale-notre-dame.jpg?w=1200&h=-1&s=1' },
        { id: '15', name: 'Cathédrale de strasbourg', cityImage: 'https://i.lepelerin.com/800x450/smart/2020/07/20/502852hr.jpg' },
      ],
      restaurants: [
        { id: '1', name: 'L\'Atelier des Augustins', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '2', name: 'Les Apothicaires', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '3', name: 'La Mère Brazier', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '4', name: 'Les Apothicaires', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
        { id: '5', name: 'La Mère Brazier', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' }
      ]
    },
    {
      id: '2',
      regionName: 'Bourgogne-Franche-Comté',
      cityName: 'Dijon',
      imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg',
      culturalPlaces: [
        { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
        { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
        { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
      ],
      restaurants: [
        { id: '1', name: 'L\'Essentiel', imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '2', name: 'Le Pré aux Clercs' },
        { id: '3', name: 'La Maison des Cariatides' }
      ]
    },
    {
      id: '3',
      regionName: 'Bretagne',
      cityName: 'Rennes',
      imageUri: 'https://www.lehavreseinemetropole.fr/sites/default/files/media/images/citenumerique-lehavre-enseignementsuperieur-innovation.jpg',
      culturalPlaces: [
        {
          "id": "1",
          "name": "Musée des Beaux-Arts de Dijon"
        },
        {
          "id": "2",
          "name": "Palais des Ducs et des États de Bourgogne"
        },
        {
          "id": "3",
          "name": "Cathédrale Saint-Bénigne de Dijon"
        }
      ],
      restaurants: [
        {
          "id": "1",
          "name": "L'Essentiel"
        },
        {
          "id": "2",
          "name": "Le Pré aux Clercs"
        },
        {
          "id": "3",
          "name": "La Maison des Cariatides"
        }
      ]
    },
    {
      id: '4',
      regionName: 'Centre-Val de Loire',
      cityName: 'Orléans',
      imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/5_monuments_historiques_a___visiter_en_france.jpg',
      culturalPlaces: [
        {
          "id": "1",
          "name": "Musée des Beaux-Arts de Dijon"
        },
        {
          "id": "2",
          "name": "Palais des Ducs et des États de Bourgogne"
        },
        {
          "id": "3",
          "name": "Cathédrale Saint-Bénigne de Dijon"
        }
      ],
      restaurants: [
        {
          "id": "1",
          "name": "L'Essentiel"
        },
        {
          "id": "2",
          "name": "Le Pré aux Clercs"
        },
        {
          "id": "3",
          "name": "La Maison des Cariatides"
        }
      ]
    },
    {
      id: '5',
      regionName: 'Corse',
      cityName: 'Ajaccio',
      imageUri: 'https://keewego.com/wp-content/uploads/2018/10/Arc-de-triomphe-11-768x512.jpg',
      culturalPlaces: [
        {
          "id": "1",
          "name": "Musée des Beaux-Arts de Dijon"
        },
        {
          "id": "2",
          "name": "Palais des Ducs et des États de Bourgogne"
        },
        {
          "id": "3",
          "name": "Cathédrale Saint-Bénigne de Dijon"
        }
      ],
      restaurants: [
        {
          "id": "1",
          "name": "L'Essentiel"
        },
        {
          "id": "2",
          "name": "Le Pré aux Clercs"
        },
        {
          "id": "3",
          "name": "La Maison des Cariatides"
        }
      ]
    },
    {
      id: '6',
      regionName: 'Grand Est',
      cityName: 'Strasbourg',
      imageUri: 'https://foulecity.com/wp-content/uploads/2018/01/visite-virtuelle-monument-historique.jpg',
      culturalPlaces: [
        {
          "id": "1",
          "name": "Musée des Beaux-Arts de Dijon"
        },
        {
          "id": "2",
          "name": "Palais des Ducs et des États de Bourgogne"
        },
        {
          "id": "3",
          "name": "Cathédrale Saint-Bénigne de Dijon"
        }
      ],
      restaurants: [
        {
          "id": "1",
          "name": "L'Essentiel"
        },
        {
          "id": "2",
          "name": "Le Pré aux Clercs"
        },
        {
          "id": "3",
          "name": "La Maison des Cariatides"
        }
      ]
    },
    {
      id: '7',
      regionName: 'Hauts-de-France',
      cityName: 'Lille',
      imageUri: 'https://picsum.photos/id/243/200/300',
      culturalPlaces: [
        { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
        { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
        { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
      ],
      restaurants: [
        { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '2', name: 'Le Pré aux Clercs' },
        { id: '3', name: 'La Maison des Cariatides' }
      ]
    },
    {
      id: '8',
      regionName: 'Île-de-France',
      cityName: 'Paris',
      imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-1.jpg',
      culturalPlaces: [
        { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
        { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
        { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
      ],
      restaurants: [
        { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '2', name: 'Le Pré aux Clercs' },
        { id: '3', name: 'La Maison des Cariatides' }
      ]
    },
    {
      id: '9',
      regionName: 'Normandie',
      cityName: 'Rouen',
      imageUri: 'https://a.cdn-hotels.com/gdcs/production139/d1145/9d3a4af5-34c1-4bb4-baa3-6096e7291ebd.jpg',
      culturalPlaces: [
        { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
        { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
        { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
      ],
      restaurants: [
        { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '2', name: 'Le Pré aux Clercs', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '3', name: 'La Maison des Cariatides', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
      ]
    },
    {
      id: '10',
      regionName: 'Nouvelle-Aquitaine',
      cityName: 'Bordeaux',
      imageUri: 'https://images.france.fr/zeaejvyq9bhj/2AIPIRpD49bBDj5351aSgi/37993a373b371a80c0ee563548cf870f/ok-Pierrefonds_nord-est__Christian_Gluckman_CMN.JPG?w=1120&h=490&q=70&fl=progressive&fit=fill',
      culturalPlaces: [
        { id: '1', name: 'Musée des Beaux-Arts de Dijon', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '2', name: 'Palais des Ducs et des États de Bourgogne', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' }
      ],
      restaurants: [
        { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '2', name: 'Le Pré aux Clercs', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
        { id: '3', name: 'La Maison des Cariatides', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' }
      ]
    },
    {
      id: '11',
      regionName: 'Normandie',
      cityName: 'Rouen',
      imageUri: 'https://i-det.unimedias.fr/sites/art-de-vivre/files/styles/large/public/dt158_yport_cote_bateau_br.jpg?auto=compress%2Cformat&crop=faces%2Cedges&cs=srgb&fit=crop&h=598&w=900',
      culturalPlaces: [
        { id: '1', name: 'Cathédrale Notre-Dame', cityImage: 'https://www.logishotels.com/images/regions/logis-hotels-mont-saint-michel.jpeg' },
        { id: '2', name: 'Le Gros-Horloge', cityImage: 'https://i-det.unimedias.fr/sites/art-de-vivre/files/styles/large/public/dt158_yport_cote_bateau_br.jpg?auto=compress%2Cformat&crop=faces%2Cedges&cs=srgb&fit=crop&h=598&w=900' },
        { id: '3', name: 'Musée des Beaux-Arts', cityImage: 'https://www.lalibre.be/resizer/aaD6MbDB1OtXVZlggTXAFCpdPx4=/1200x800/filters:format(jpeg):focal(465x240:475x230)/cloudfront-eu-central-1.images.arcpublishing.com/ipmgroup/XVYKY5EMYRBGDFXD5FD27IWU7U.jpg' },
        { id: '4', name: 'Château de Robert le Diable' },
        { id: '5', name: 'Abbatiale Saint-Ouen' },
      ],
      restaurants: [
        { id: '1', name: 'La Couronne' },
        { id: '2', name: 'Gill' },
      ]
    }
  ];

  // Fonction pour générer les données aléatoires
  const generateExtraData = () => {
    if (isLoading) return;
    setIsLoading(true);
    setTimeout(() => {
      const newData = [...Array(5)].map((_, index) => ({
        id: `random-${index}-${Date.now()}`,
        imageUri: 'https://justinebriot.fr/wp-content/uploads/2021/10/BlogLeHavre-scaled-e1633542971170.jpg',
        regionName: 'Random Region',
        cityName: 'Random City',
      }));
      setExtraData((prevExtraData) => {
        const mergedData = [...prevExtraData, ...newData];
        const uniqueData = mergedData.filter(
          (value, index, self) => self.findIndex((v) => v.id === value.id) === index
        );
        return uniqueData;
      });
      setIsLoading(false);
    }, 1500);
  };

  const Item = ({ item, onPress, isSelected }) => (
    <TouchableOpacity onPress={onPress} style={[styles.item, styles.regionItem, isSelected && styles.selectedItem]}>
      <Image
        source={{ uri: item.imageUri }}
        style={{ width: '100%', height: '75%', borderRadius: 10 }}
      />
      <View style={{ padding: 10 }}>
        <Text style={{ fontWeight: 'bold', color: '#00008b'}}>{item.regionName}</Text>
        <Text style={styles.cityName}>{item.cityName}</Text>
      </View>
    </TouchableOpacity>
  );

  const [selectedId, setSelectedId] = useState(null);
  const renderItem = ({ item }) => {
    const isSelected = item.id === selectedId;
    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          navigation.navigate('Root', { screen: 'Explore', params: { region: item } });
        }}
        isSelected={isSelected}
      />
    );
  };

  const handleScroll = (event) => {
    const { x } = event.nativeEvent.contentOffset;
    setIsScrolling(x > 0);
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={regionsData}
        renderItem={(props) =>
          renderItem({ ...props, onPress: setSelectedId, selectedId })
        }
        keyExtractor={(item) => item.id}
        extraData={selectedId}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.contentContainerStyle}
        // onEndReached={generateExtraData}
        // onEndReachedThreshold={0.5}
        ListFooterComponent={
          isLoading ? (
            <View style={styles.spinner}>
              <ActivityIndicator size="large" color="blue" />
            </View>
          ) : null
        }
      />
    </View>
  );

}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10,
    // position: 'absolute',
    // top: 200, // ajustez la position verticale souhaitée
    zIndex: 2, // pour afficher la FlatList au-dessus de l'image
  },
  contentContainerStyle: {
    paddingHorizontal: 16,
  },
  item: {
    zIndex: 1,
    width: width / 1.5,
    height: 200,
    borderRadius: 22,
    padding: 12,
    marginRight: 16,
    marginBottom: 16,
    backgroundColor: 'white',
    shadowColor: '#a9a9a9',
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2
  },
  selectedItem: {
    backgroundColor: '#f0edf6',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  selectedTitle: {
    color: '#fff',
  },
  city: {
    fontSize: 18,
    marginTop: 10,
    paddingHorizontal: 5,
   
  },
  cityName: {
    fontSize: 'small',
    color: 'gray',
    paddingTop: 5,
    paddingHorizontal: 5
  }
});
