import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();

export default function MenuRoot() {

    const navigation = useNavigation()
    const [isMenuOpen, setIsMenuOpen] = useState(false);

    const goToRegions = () => {
        console.log('go to regions')
        navigation.navigate('Root', { screen: 'OurRegions' });
    }

    const goToParcour = () => {
        console.log('go to parcour')
        navigation.navigate('Root', { screen: 'Mon Itineraire' });
    }
    const goToAccount = () => {
        console.log('go to account')
        navigation.navigate('Root', { screen: 'Mon compte' });
    }

    const goToLogin = () => {
        console.log('Déconnexion')
        navigation.navigate('Login');
    }

    const handleMenuPress = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    const handleOverlayPress = () => {
        setIsMenuOpen(false);
    }

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={handleMenuPress}>
                <MaterialIcons name="menu-open" size={30} color="black" />
            </TouchableOpacity>

            {isMenuOpen && (
                <TouchableWithoutFeedback onPress={handleOverlayPress}>
                    <View style={styles.menu}>
                        <TouchableOpacity onPress={handleMenuPress} style={styles.closeButton}>
                            <Ionicons name="close-outline" size={28} color="black" />
                        </TouchableOpacity>

                        <View style={styles.menuItems}>
                            <TouchableOpacity style={styles.menuItem}>
                                <Text onPress={goToRegions} style={styles.menuItemText}>Régions</Text>
                            </TouchableOpacity>

                            {/* <TouchableOpacity style={styles.menuItem}>
                                <Text style={styles.menuItemText}>Explore</Text>
                            </TouchableOpacity> */}

                            <TouchableOpacity style={styles.menuItem}>
                                <Text onPress={goToParcour} style={styles.menuItemText}>Mon parcours</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.menuItem}>
                                <Text onPress={goToAccount} style={styles.menuItemText}>Mon compte</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.menuItem}>
                                <Text onPress={goToLogin} style={styles.menuItemText}>Déconnexion</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </TouchableWithoutFeedback>
            )}
        </View>
    );
}

const { width } = Dimensions.get("window")
const styles = StyleSheet.create({
    container: {
      zIndex: 1,
      backgroundColor: 'transparent',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 20,
      paddingVertical: 10,
    },
    menu: {
      zIndex: 2,
      backgroundColor: 'transparent',
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      backgroundColor: '#eee',
      justifyContent: 'center',
      alignItems: 'unset',
    },
    closeButton: {
      position: 'absolute',
      top: 20,
      right: 20,
    },
    menuItems: {
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 100,
      paddingTop: 100,
      width: width * 0.8,
      marginLeft: width * 0.05,
      marginBottom: -15,
      borderRadius: 15,
      backgroundColor: '#eee',
      padding: 10,
  
    },
    menuItem: {
      paddingVertical: 10,
      borderBottomWidth: 1,
      borderBottomColor: '#ccc',
      width: '100%',
    },
    menuItemText: {
      fontSize: width < 375 ? 16 : 18,
      color: '#333',
    },
  });





// Responsive Menu..

// const { width, height } = Dimensions.get("window");
// const styles = StyleSheet.create({
//   container: {
//     zIndex: 1,
//     backgroundColor: 'transparent',
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//     paddingHorizontal: 0.05 * width,
//     paddingVertical: 0.02 * height,
//   },
//   menu: {
//     zIndex: 2,
//     backgroundColor: 'transparent',
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     width: '100%',
//     height: '100%',
//     backgroundColor: '#eee',
//     justifyContent: 'center',
//     alignItems: 'unset',
//   },
//   closeButton: {
//     position: 'absolute',
//     top: 0.03 * height,
//     right: 0.03 * width,
//   },
//   menuItems: {
//     justifyContent: 'center',
//     alignItems: 'center',
//     marginTop: 0.15 * height,
//     paddingTop: 0.15 * height,
//     width: 0.75 * width,
//     marginLeft: 0.05 * width,
//     marginBottom: -0.05 * height,
//     borderRadius: 0.03 * height,
//     backgroundColor: '#eee',
//     padding: 0.02 * height,
//   },
//   menuItem: {
//     paddingVertical: 0.02 * height,
//     borderBottomWidth: 1,
//     borderBottomColor: '#ccc',
//     width: '100%',
//   },
//   menuItemText: {
//     fontSize: 0.04 * height,
//     color: '#333',
//   },
// });