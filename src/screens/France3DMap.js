// import React from "react";
// import { GLView } from "expo-gl";
// import { Renderer, THREE } from "expo-three";
// import FranceModel from "../Models/FranceModel";


// export default function France3DMap() {
//     const onContextCreate = async (gl) => {
//       const renderer = new Renderer({ gl });
//       renderer.setSize(gl.drawingBufferWidth, gl.drawingBufferHeight);
  
//       const scene = new THREE.Scene();
//       const camera = new THREE.PerspectiveCamera(75, gl.drawingBufferWidth / gl.drawingBufferHeight, 0.1, 1000);
//       camera.position.z = 5;
  
//       await FranceModel(scene);
  
//       const animate = () => {
//         requestAnimationFrame(animate);
//         renderer.render(scene, camera);
//         gl.endFrameEXP();
//       };
//       animate();
//     };
  
//     return <GLView style={{ flex: 1 }} onContextCreate={onContextCreate} />;
//   }
  
