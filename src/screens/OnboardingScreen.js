import React from "react";
// import { useNavigation } from '@react-navigation/native';
import AppIntroSlider from "react-native-app-intro-slider";
import { Ionicons } from "react-native-vector-icons";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";

const slides = [
  {
    key: "1",
    title: "Sélectionnez votre itinéraire",
    subtitle: "Découvrez les meilleures attractions autour de vous.",
    image: require("../../assets/map.jpg"),
    backgroundColor: "#d3d3d3",
  },
  {
    key: "2",
    title: "Sélectionnez votre itinéraire",
    subtitle: "Planifiez votre journée à votre rythme.",
    image: require("../../assets/map2.jpg"),
    backgroundColor: "#d3d3d3",
  },
  {
    key: "3",
    title: "Profitez de votre voyage",
    subtitle:
      "Nous nous assurons que vous ne manquez pas les meilleures expériences.",
    image: require("../../assets/map4.jpg"),
    backgroundColor: "#22bcb5",
  },
  // {
  //     key: '4',
  //     title: 'Rocket guy',
  //     text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
  //     image: require('../../assets/gradient/1.png'),
  //     backgroundColor: '#22bcb5',
  // }
];

export default class OnboardingScreen extends React.Component {
  _renderItem = ({ item }) => {
    return (
      <View style={styles.slide}>
        <Image source={item.image} style={styles.image} />
        <Text style={styles.text}>{item.text}</Text>
        <Text style={styles.text}>{item.subtitle}</Text>
      </View>
    );
  };

  _renderButton = (name) => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons name={name} color="rgba(255, 255, 255, .9)" size={24} />
      </View>
    );
  };

  _renderNextButton = () => {
    return this._renderButton("md-arrow-forward-outline");
  };

  _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Ionicons
          name="md-checkmark-done"
          color="rgba(255, 255, 255, .9)"
          size={24}
          onPress={this.props.handleDone} // Use the function passed from the parent component
        />
      </View>
    );
  };

  render() {
    return (
      <AppIntroSlider
        renderItem={this._renderItem}
        data={slides}
        activeDotStyle={{
          backgroundColor: "#21465b",
          width: 25,
        }}
        renderDoneButton={this._renderDoneButton}
        renderNextButton={this._renderNextButton}
      />
    );
  }
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  slide: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#d3d3d3",
  },
  title: {
    fontSize: width < 350 ? 13 : 15, // Adjust size for smaller devices
    color: "#333",
    textAlign: "center",
    paddingHorizontal: 30,
  },
  text: {
    color: "#fff",
    textAlign: "center",
    paddingBottom: 10,
    fontSize: width < 350 ? 20 : 23, // Adjust size for smaller devices
    fontWeight: "bold",
    alignSelf: "center",
    position: "absolute",
    fontFamily: 'poppins',
    bottom: 50,
    paddingHorizontal: 4,
  },
  image: {
    width: width,
    height: height, // use percentage instead of fixed height
    resizeMode: "cover",
  },
  buttonCircle: {
    width: width * 0.1, // use percentage of width
    height: width * 0.1, // use percentage of width
    backgroundColor: "rgba(0, 0, 0, .2)",
    borderRadius: (width * 0.1) / 2, // use percentage of width
    justifyContent: "center",
    alignItems: "center",
  },
});
