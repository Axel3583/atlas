import React from "react";
import { StyleSheet, Text, View, Dimensions, Image } from "react-native";


export default function LoginScreen(){

  return(
    <View style={styles.halfScreenBackground}>
    <Image source={require('../../assets/map.jpg')} style={styles.backgroundImage} />
  </View>
  )
}

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  halfScreenBackground: {
    height: height, // ajustez la hauteur souhaitée de l'image
    overflow: 'hidden', // masque le contenu qui dépasse de la vue
    width: width
  },
  backgroundImage: {
    flex: 1,
    width: width,
    height: height,
    resizeMode: 'cover',
  },
})