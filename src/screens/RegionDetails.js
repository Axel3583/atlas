import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Switch,
} from "react-native";
import SearchBar from "../components/SearchBar";
import Title from "../components/Title";
import ImageModal from "../components/Modals";
import { FidgetSpinner } from "react-loader-spinner";
import { useNavigation } from "@react-navigation/native";
import { MaterialIcons } from "@expo/vector-icons";
import { FontAwesome } from "@expo/vector-icons";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { FontAwesome5 } from "@expo/vector-icons";
import InterestSelectionScreen from "./InterestSelectionScreen";
import { LinearGradient } from "expo-linear-gradient";

export default function RegionDetails({ route }) {
  const { region } = route.params;
  const [selectedItem, setSelectedItem] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [extraData, setExtraData] = useState(region.culturalPlaces.slice(0, 5)); // 1. Initialize extraData with the first 5 culturalPlaces
  const [isLoading, setIsLoading] = useState(false);
  const [isScrolling, setIsScrolling] = useState(false);
  const [dataLimit, setDataLimit] = useState(5);
  const [savedItems, setSavedItems] = useState([]);
  const [showDeleteButton, setShowDeleteButton] = useState(false);
  const [showPressable, setShowPressable] = useState(true);
  const [isFavorited, setIsFavorited] = useState(false);
  const [showInterestSelection, setShowInterestSelection] = useState(false);
  const [showContent, setShowContent] = useState(false); // Nouvel état
  const [selectedRegion, setSelectedRegion] = useState(null); // Ajoutez cette ligne
  const [showInterestButton, setShowInterestButton] = useState(true);

  const navigation = useNavigation();

  const handleOpenModal = (item) => {
    setSelectedItem(item);
    setModalVisible(true);
    setShowPressable(true);
    setShowDeleteButton(false);
  };

  const handleGenerateItinerary = () => {
    navigation.navigate("Root", {
      screen: "Mon Itineraire",
      params: { selectedItem, selectedItem },
    });
  };

  // const handleSaveItem = (item) => {
  //   if (savedItems.includes(item.id)) {
  //     setSavedItems(savedItems.filter((itemId) => itemId !== item.id));
  //   } else {
  //     setSavedItems([...savedItems, item.id]);
  //   }
  // };

  const handleAddToCart = (item) => {
    if (!savedItems.includes(item.id)) {
      setSavedItems([...savedItems, item]);
    }
  };

  const handleRemoveFromCart = (itemId) => {
    setSavedItems(savedItems.filter((item) => item.id !== itemId));
  };

  const handleCloseModal = () => {
    setSelectedItem(null);
    setModalVisible(false);
  };

  const handleScroll = (event) => {
    const { x } = event.nativeEvent.contentOffset;
    setIsScrolling(x > 0);
  };

  const generateExtraData = () => {
    if (isLoading) return;
    setIsLoading(true);
    setTimeout(() => {
      const newData = region.culturalPlaces.slice(dataLimit, dataLimit + 5);
      setExtraData((prevData) => [...prevData, ...newData]);
      setDataLimit(dataLimit + 5);
      setIsLoading(false);
    }, 1500);
  };

  const toggleFavorite = () => {
    setIsFavorited(!isFavorited);
  };

  const renderItem = ({ item }) => {
    const isFavorited = savedItems.includes(item.id);
    return (
      <TouchableOpacity onPress={() => handleOpenModal(item)}>
        <View style={styles.placeContainer}>
          <View style={styles.cityImg}>
            <Image source={{ uri: item.cityImage }} style={styles.cityImg} />
          </View>
          <View style={styles.placeNameContainer}>
            <Text style={styles.placeName}>{item.name}</Text>
          </View>

          <View style={styles.iconContainer}>
            <TouchableOpacity onPress={() => toggleFavorite()}>
              <FontAwesome
                name={isFavorited ? "heart" : "heart-o"}
                size={24}
                color={isFavorited ? "#F44336" : "#f5f5f5"} // change the color according to your preference
              />
            </TouchableOpacity>
          </View>

          <View style={styles.switchContainer}>
            <Switch
              style={styles.switch}
              trackColor={{ false: "#767577", true: "#81b0ff" }}
              thumbColor={savedItems.includes(item.id) ? "white" : "#f4f3f4"}
              ios_backgroundColor="#3e3e3e"
              value={savedItems.includes(item.id)}
              onValueChange={() => handleSaveItem(item)}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  // const generateExtraData = () => {
  //     if (isLoading) return;
  //     setIsLoading(true);
  //     setTimeout(() => {
  //       const newData = region.culturalPlaces.slice(dataLimit, dataLimit + 5);
  //       setExtraData((prevExtraData) => {
  //         const newExtraData = [...prevExtraData];
  //         newExtraData.splice(0, newData.length, ...newData);
  //         return newExtraData;
  //       });
  //       setDataLimit(dataLimit + 5);
  //       setIsLoading(false);
  //       if (extraData.length === 0) {
  //         setIsLoading(false);
  //       }
  //     }, 1500);
  //   };

  // return (
  //   <View style={styles.container}>
  //     <View style={styles.flat}>
  //       <SearchBar />
  //     </View>

  //     <View style={styles.cartContainer}>
  //       {/* <Text style={styles.cartTitle}>Parcours</Text> */}
  //       <MaterialCommunityIcons
  //         style={styles.cartTitle}
  //         name="map-marker-distance"
  //         size={24}
  //         color="black"
  //       />
  //       {savedItems.length > 0 ? (
  //         <>
  //           <FlatList
  //             style={{ flexGrow: 1 }}
  //             data={savedItems}
  //             keyExtractor={(item) => item.id.toString()}
  //             renderItem={({ item }) => (
  //               <View style={styles.cartItem}>
  //                 <Text style={styles.cartItemName}>{item.name}</Text>
  //                 <TouchableOpacity
  //                   style={styles.cartRemoveButton}
  //                   onPress={() => handleRemoveFromCart(item.id)}
  //                 >
  //                   <MaterialIcons name="delete" size={24} color="white" />
  //                 </TouchableOpacity>
  //               </View>
  //             )}
  //           />
  //           <TouchableOpacity
  //             style={styles.generateButton}
  //             onPress={handleGenerateItinerary}
  //           >
  //             <Text style={styles.generateButtonText}>
  //               Faire mon itinéraire
  //             </Text>
  //             <FontAwesome5 name="route" size={24} color="black" />
  //           </TouchableOpacity>
  //         </>
  //       ) : (
  //         <Text style={styles.emptyCartText}>
  //           Votre liste de parcours est vide.
  //         </Text>
  //       )}
  //     </View>

  //     <ScrollView showsHorizontalScrollIndicator={false}>
  //     {showInterestSelection ? (
  //       <InterestSelectionScreen handleDone={() => setShowInterestSelection(false)} />
  //     ) : (
  //       <TouchableOpacity onPress={() => setShowInterestSelection(true)}>
  //         <View style={styles.cityImage}>
  //           <Image source={{ uri: region.imageUri }} style={styles.cityImage} />
  //           <View style={styles.textContainer}>
  //             <Text style={styles.regionName}>{region.regionName}</Text>
  //           </View>
  //         </View>
  //       </TouchableOpacity>
  //     )}

  //       <View style={styles.contentContainer}>
  //         <View style={{ paddingVertical: 15 }}>
  //           <Title size={"small"} content={"Restaurants"} />
  //         </View>
  //         <FlatList
  //           data={extraData}
  //           keyExtractor={(item) => item.id.toString()}
  //           horizontal={true}
  //           showsHorizontalScrollIndicator={false}
  //           onEndReached={generateExtraData}
  //           onEndReachedThreshold={0.5}
  //           ListFooterComponent={
  //             isLoading ? (
  //               <View style={styles.spinner}>
  //                 <FidgetSpinner
  //                   visible={true}
  //                   height="80"
  //                   width="80"
  //                   ariaLabel="dna-loading"
  //                   wrapperStyle={{}}
  //                   wrapperClass="dna-wrapper"
  //                   ballColors={["#191970", "#87cefa", "#1e90ff"]}
  //                   backgroundColor="#f0edf6"
  //                 />
  //               </View>
  //             ) : null
  //           }
  //           renderItem={renderItem}
  //         />

  //         <View style={{ paddingVertical: 15 }}>
  //           <Title size={"small"} content={"Activités culturelles"} />
  //         </View>
  //         <FlatList
  //           data={region.culturalPlaces}
  //           keyExtractor={(item) => item.id.toString()}
  //           horizontal={true}
  //           showsHorizontalScrollIndicator={false}
  //           renderItem={({ item }) => (
  //             <TouchableOpacity onPress={() => handleOpenModal(item)}>
  //               <View style={styles.placeContainer}>
  //                 <View style={styles.cityImg}>
  //                   <Image
  //                     source={{ uri: item.cityImage }}
  //                     style={styles.cityImg}
  //                   />
  //                 </View>
  //                 <View style={styles.placeNameContainer}>
  //                   <Text style={styles.placeName}>{item.name}</Text>
  //                 </View>
  //               </View>
  //             </TouchableOpacity>
  //           )}
  //         />
  //         <View style={{ paddingVertical: 15 }}>
  //           <Title size={"small"} content={"Autres activités"} />
  //         </View>
  //         <FlatList
  //           data={region.restaurants}
  //           keyExtractor={(item) => item.id.toString()}
  //           horizontal={true}
  //           showsHorizontalScrollIndicator={false}
  //           renderItem={({ item }) => (
  //             <TouchableOpacity onPress={() => handleOpenModal(item)}>
  //               <View style={styles.placeContainer}>
  //                 <View style={styles.cityImg}>
  //                   <Image
  //                     source={{ uri: item.cityImage }}
  //                     style={styles.cityImg}
  //                   />
  //                 </View>
  //                 <View style={styles.placeNameContainer}>
  //                   <Text style={styles.placeName}>{item.name}</Text>
  //                 </View>
  //               </View>
  //             </TouchableOpacity>
  //           )}
  //         />
  //       </View>
  //     </ScrollView>

  //     <ImageModal
  //       item={selectedItem}
  //       modalVisible={modalVisible}
  //       onClose={handleCloseModal}
  //       onAddToCart={handleAddToCart}
  //       onRemoveFromCart={handleRemoveFromCart}
  //     />
  //   </View>
  // );

  return (
    <View style={styles.container}>
      {showInterestSelection ? (
        <InterestSelectionScreen
          handleDone={() => {
            setShowInterestSelection(false);
            setShowContent(true);
          }}
        />
      ) : (
        <>
          {showContent && (
            <View style={styles.cartContainer}>
              <MaterialCommunityIcons
                style={styles.cartTitle}
                name="map-marker-distance"
                size={24}
                color="black"
              />
              {savedItems.length > 0 ? (
                <>
                  {/* <View style={styles.flat}>
                    <SearchBar />
                  </View> */}
                  <FlatList
                    style={{ flexGrow: 1 }}
                    data={savedItems}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={({ item }) => (
                      <View style={styles.cartItem}>
                        <Text style={styles.cartItemName}>{item.name}</Text>
                        <TouchableOpacity
                          style={styles.cartRemoveButton}
                          onPress={() => handleRemoveFromCart(item.id)}
                        >
                          <MaterialIcons
                            name="delete"
                            size={24}
                            color="white"
                          />
                        </TouchableOpacity>
                      </View>
                    )}
                  />
                  <TouchableOpacity
                    style={styles.generateButton}
                    onPress={handleGenerateItinerary}
                  >
                    <Text style={styles.generateButtonText}>
                      Faire mon itinéraire
                    </Text>
                    <FontAwesome5 name="route" size={24} color="black" />
                  </TouchableOpacity>
                </>
              ) : (
                <Text style={styles.emptyCartText}>
                  Votre liste de parcours est vide.
                </Text>
              )}
            </View>
          )}

          {showInterestButton && (
            <LinearGradient
              colors={["#dcdcdc", "#fff", "#f5f5f5"]}
              style={{ flex: 1 }}
            >
              <TouchableOpacity
                style={{ paddingTop: 50 }}
                onPress={() => {
                  setShowInterestSelection(true);
                  setShowInterestButton(false);
                }}
              >
                <View style={styles.cityImage}>
                  <Image
                    source={{ uri: region.imageUri }}
                    style={styles.cityImage}
                  />
                  <View style={styles.textContainer}>
                    <Text style={styles.regionName}>{region.regionName}</Text>
                  </View>
                </View>
              </TouchableOpacity>

              <View style={styles.halfScreenBackground}>
                <Image
                  source={require("../../assets/fond.jpeg")}
                  style={styles.backgroundImage}
                />
              </View>
            </LinearGradient>
          )}
        </>
      )}

      {showContent && (
        <ScrollView showsHorizontalScrollIndicator={false}>
          <View style={styles.contentContainer}>
            <View style={{ paddingVertical: 15 }}>
              <Title size={"small"} content={"Restaurants"} />
            </View>
            <FlatList
              data={extraData}
              keyExtractor={(item) => item.id.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              onEndReached={generateExtraData}
              onEndReachedThreshold={0.5}
              ListFooterComponent={
                isLoading ? (
                  <View style={styles.spinner}>
                    <FidgetSpinner
                      visible={true}
                      height="80"
                      width="80"
                      ariaLabel="dna-loading"
                      wrapperStyle={{}}
                      wrapperClass="dna-wrapper"
                      ballColors={["#191970", "#87cefa", "#1e90ff"]}
                      backgroundColor="#f0edf6"
                    />
                  </View>
                ) : null
              }
              renderItem={renderItem}
            />

            <View style={{ paddingVertical: 15 }}>
              <Title size={"small"} content={"Activités culturelles"} />
            </View>
            <FlatList
              data={region.culturalPlaces}
              keyExtractor={(item) => item.id.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => (
                <TouchableOpacity onPress={() => handleOpenModal(item)}>
                  <View style={styles.placeContainer}>
                    <View style={styles.cityImg}>
                      <Image
                        source={{ uri: item.cityImage }}
                        style={styles.cityImg}
                      />
                    </View>
                    <View style={styles.placeNameContainer}>
                      <Text style={styles.placeName}>{item.name}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
            <View style={{ paddingVertical: 15 }}>
              <Title size={"small"} content={"Autres activités"} />
            </View>
            <FlatList
              data={region.restaurants}
              keyExtractor={(item) => item.id.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={({ item }) => (
                <TouchableOpacity onPress={() => handleOpenModal(item)}>
                  <View style={styles.placeContainer}>
                    <View style={styles.cityImg}>
                      <Image
                        source={{ uri: item.cityImage }}
                        style={styles.cityImg}
                      />
                    </View>
                    <View style={styles.placeNameContainer}>
                      <Text style={styles.placeName}>{item.name}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
        </ScrollView>
      )}

      <ImageModal
        item={selectedItem}
        modalVisible={modalVisible}
        onClose={handleCloseModal}
        onAddToCart={handleAddToCart}
        onRemoveFromCart={handleRemoveFromCart}
      />
    </View>
  );
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    height: "100%",
    // backgroundColor: 'red'
  },

  textContainer: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: [{ translateX: "-50%" }, { translateY: "-50%" }],
    // backgroundColor: "rgba(255, 255, 255, 0.8)",
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 10,
  },
  regionName: {
    fontFamily: "Poppins",
    fontSize: 14,
    color: "#fff",
  },
  cartContainer: {
    paddingBottom: 3,
    paddingBottom: 25,
    backgroundColor: "#fff",
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    margin: 5,
  },
  cartTitle: {
    paddingHorizontal: 10,
    fontSize: 30,
    fontWeight: "bold",
    color: "#333",
    marginBottom: 20,
  },
  halfScreenBackground: {
    height: 270, // ajustez la hauteur souhaitée de l'image
    overflow: "hidden", // masque le contenu qui dépasse de la vue
    top: 235,
  },
  backgroundImage: {
    flex: 1,
    width: "100%",
    resizeMode: "cover",
    aspectRatio: 1,
  },
  cartItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    marginHorizontal: 10,
    marginBottom: 10,
    shadowColor: "#000",
    borderRadius: 20,
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.2,
    // shadowRadius: 1.41,
    // elevation: 2,
  },
  cartItemName: {
    borderRadius: 20,
    marginHorizontal: 5,
    fontSize: 15,
    color: "#333",
    fontFamily: "poppins",
    alignItems: "center",
    backgroundColor: "#f0edf6",
    paddingHorizontal: width * 0.03,
    paddingVertical: height * 0.015,
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 2,
  },
  cartRemoveButton: {
    backgroundColor: "#f44336",
    padding: 10,
    borderRadius: 20,
  },
  generateButton: {
    backgroundColor: "rgba(91, 91, 102, 1)",
    padding: 15,
    margin: 10,
    borderRadius: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  generateButtonText: {
    color: "#fff",
    fontSize: 15,
    fontWeight: "medium",
    fontFamily: "poppins",
  },
  emptyCartText: {
    fontSize: 15,
    color: "#333",
    textAlign: "center",
    fontFamily: "poppins",
  },
  switchContainer: {
    top: 240,
    position: "absolute",
    paddingTop: 255,
    left: 10,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  switch: {
    shadowColor: "black",
    backgroundColor: "white",
    shadowOpacity: 10,
    color: "blue",
    paddingHorizontal: 5,
    fontSize: 14,
  },
  spinner: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    flexDirection: "row",
  },

  cityImage: {
    width: "100%",
    height: 180,
    borderRadius: 10,
    paddingTop: 10,
    paddingHorizontal: 7,
    shadowColor: "#f0edf6",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 5,
    shadowRadius: 1,
    borderRadius: 20,
    // opacity: 0.8,
  },

  placeContainer: {
    position: "relative",
    width: 200, // ajuster la largeur en fonction de votre mise en page
    height: 260, // ajuster la hauteur en fonction de votre mise en page
    margin: 10, // ajuster la marge en fonction de votre mise en page
    shadowColor: "#000",
    borderRadius: 30,
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  iconContainer: {
    position: "absolute",
    top: 0,
    left: 10,
    width: 177,
    paddingTop: 10,
    alignItems: "flex-end",
  },
  placeNameContainer: {
    position: "absolute",
    left: 10,
    paddingTop: 207,
    width: 198,
  },
  placeName: {
    shadowColor: "black",
    shadowOpacity: 10,
    color: "white",
    paddingHorizontal: 5,
    fontSize: 14, // ajuster la taille de la police en fonction de votre mise en page
  },
  cityImg: {
    flex: 1,
    backgroundColor: "#f5f5f5",
    shadowColor: "#f0edf6",
    // shadowOffset: { width: 0, height: 0.0 },
    shadowOpacity: 5,
    // shadowRadius: 1,
    // height: 100,
    borderRadius: 20,
  },
  contentContainer: {
    flex: 1,
    padding: 20,
    height: height,
  },
  placeItem: {
    flex: 1,
    marginBottom: 20,
    width: "100%",
  },
  placeDescription: {
    fontSize: 16,
  },
  restaurantItem: {
    marginBottom: 20,
  },
  restaurantName: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
  },
  restaurantDescription: {
    fontSize: 16,
  },
});
