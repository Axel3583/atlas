import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { Ionicons } from '@expo/vector-icons'

export default function RoundTabBar({iconName, onPress}) {

    return (
        <TouchableOpacity onPress={onPress} style={styles.container}>
            <View style={styles.container}>
                <Ionicons style={styles.icon} name={iconName} />
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'colum',
    },
    icon: {
        height: 5
    }
})