import React from "react";
import { StyleSheet, View, Text, Dimensions } from "react-native";

export default function Title({ content, size }) {
  const { container, big, small, medium } = styles;

  const goToStyles = () => {
    switch (size) {
      case "big":
        return big;
      case "small":
        return small;
      case "medium":
        return medium;
    }
  };

  return (
    <View style={container}>
      <Text style={goToStyles()}>{content}</Text>
    </View>
  );
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flexDirection: "column",
    justifyContent: "center",
    maxHeight: 90,
  },
  big: {
    paddingVertical: height * 0.05,
    fontFamily: "LeckerliOne",
    color: "back",
    fontWeight: "bold",
    fontSize: width * 0.09,
    paddingHorizontal: width * 0.05,
  },
  small: {
    color: "black",
    fontFamily: "Poppins",
    fontSize: 15,
    fontWeight: "300",
    lineHeight: 28,
  },
  medium: {
    fontFamily: "Poppins",
    fontWeight: "550",
    paddingLeft: 28,
    width: width,
    padding: 14,
    color: '#000080'
    // paddingHorizontal: width * 0.05
  },

  //      textStyle: {
  //     fontWeight: 'bold',
  //     fontSize: width * 0.08,
  //     padding: width * 0.05,
  //   },
  //   text: {
  //     paddingBottom: height * 0.03,
  //     fontWeight: 'medium',
  //     fontSize: width * 0.05,
  //     paddingHorizontal: width * 0.05,
  //   },
  // })
});
