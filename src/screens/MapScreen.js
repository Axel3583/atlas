import { Text, StyleSheet, View, Image, Dimensions, TouchableOpacity } from 'react-native'
import React from 'react'
import SelectList from '../components/SelectList'
import { ScrollView } from 'react-native-gesture-handler'
// import { Onboarding } from 'stepper-react-native'


export default function MapScreen() {

  return (
    <ScrollView
    showsHorizontalScrollIndicator={false}
    >
      <View style={styles.container}>
        <View>
          <Image style={styles.image} source={{ uri: 'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/13/f2/d1/57/la-plage-du-havre.jpg?w=1200&h=1200&s=1' }} />
        </View>

        <View style={styles.item}>
          <Text style={styles.itemStyle}>
            Créer votre itinéraire grâce à nos choix incontournables
          </Text>
          <View>
            <SelectList />
          </View>
        </View>

        <View style={styles.Mapscontainer}>
          <Image style={styles.MapStyle} source={{ uri: 'https://preview.free3d.com/img/2017/02/2154853997301729124/owrl18so.jpg' }} />
        </View>
      </View>
    </ScrollView>
  )
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    flexDirection: 'colum',
    justifyContent: 'center',
    backgroundColor: '#0000'
  },
  image: {
    width: width,
    height: 250,
    borderBottomRightRadius: 25,
    borderBottomLeftRadius: 25,
    backgroundColor: '#f5f5f5',
    shadowColor: '#f0edf6'
  },
  Mapscontainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  MapStyle: {
    width: width,
    height: 220
  },
  item: {
    paddingVertical: 30
  },
  itemStyle: {
    textAlign: 'center',
    fontSize: 'large',
    fontWeight: 100,
    fontFamily: "Poppins",
  }
})