import { StyleSheet, Text, View, Dimensions, Image } from "react-native";
import React, { useState } from "react";
import MenuRoot from "../components/MenuRoot";
import SearchBar from "../components/SearchBar";
import FlatList from "../components/RegionList";
import ThreeEarth from "../components/ThreeRain";
import Title from "../components/Title";
import { LinearGradient } from "expo-linear-gradient";
import ModalComponent from "../components/ModalPopUp";

export default function HomeScreen() {
  const [modalVisible, setModalVisible] = useState(true);

  return (
    <>
      <ModalComponent
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
      />

      <LinearGradient
        colors={["#dcdcdc", "#fff", "#f5f5f5"]}
        style={{ flex: 1 }}
      >
        <View style={styles.container}>
          <View style={styles.halfScreen}>
            <MenuRoot />
            <Text style={styles.textStyle}>Trouve ton prochain souvenir</Text>
            <Title
              size={"medium"}
              content={"Ne manquez jamais rien autour de vous."}
            />
            <SearchBar />
            <FlatList />
            {/* <MenuRoot />
    <Text style={styles.textStyle}>Trouve ton prochain souvenir</Text>
    <Title size={'medium'} content={'Ne manquez jamais rien autour de vous.'} />
    <SearchBar />
    <FlatList /> */}
          </View>
          <View style={styles.halfScreenBackground}>
            <Image
              source={require("../../assets/fond.jpeg")}
              style={styles.backgroundImage}
            />
          </View>
        </View>
      </LinearGradient>
    </>
  );

  // return (
  //   // <ThreeEarth>
  //   //   <MenuRoot />
  //   //   <Title size={'big'} content=" Trouve ton prochain souvenir" />
  //   //   <Title size={'medium'} content=" Ne manquez jamais rien autour de vous." />
  //   //   <SearchBar />
  //   //   <FlatList />
  //   // </ThreeEarth>
  //   <LinearGradient colors={["#dcdcdc", "#fff", "#f5f5f5"]} style={{ flex: 1 }}>
  //     <View style={styles.container}>
  //       <View style={styles.halfScreen}>
  //         <MenuRoot />
  //         <Text style={styles.textStyle}>Trouve ton prochain souvenir</Text>
  //         <Title
  //           size={"medium"}
  //           content={"Ne manquez jamais rien autour de vous."}
  //         />
  //         <SearchBar />
  //         <FlatList />
  //         {/* <MenuRoot />
  //   <Text style={styles.textStyle}>Trouve ton prochain souvenir</Text>
  //   <Title size={'medium'} content={'Ne manquez jamais rien autour de vous.'} />
  //   <SearchBar />
  //   <FlatList /> */}
  //       </View>
  //       <View style={styles.halfScreenBackground}>
  //         <Image
  //           source={require("../../assets/fond.jpeg")}
  //           style={styles.backgroundImage}
  //         />
  //       </View>
  //     </View>
  //   </LinearGradient>
  // );
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  halfScreen: {
    flex: 1,
    zIndex: 1, // pour empiler les composants au-dessus de l'image
  },
  textStyle: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  halfScreenBackground: {
    height: 270, // ajustez la hauteur souhaitée de l'image
    overflow: "hidden", // masque le contenu qui dépasse de la vue
  },
  backgroundImage: {
    flex: 1,
    width: "100%",
    resizeMode: "cover",
    aspectRatio: 1,
  },
  textStyle: {
    fontWeight: "medium",
    fontSize: width * 0.1,
    padding: width * 0.05,
    color: "#191970",
  },
  text: {
    paddingBottom: height * 0.03,
    fontWeight: "medium",
    fontSize: width * 0.05,
    paddingHorizontal: width * 0.05,
  },
});
