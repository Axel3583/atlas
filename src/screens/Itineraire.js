import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  ScrollView,
} from "react-native";
import Collapsible from "react-native-collapsible";
import Icon from "react-native-vector-icons/Ionicons";
import Timeline from "react-native-timeline-flatlist";
import Draggable from "react-native-draggable";

const SECTIONS = [
  {
    id: 1,
    title: "Destination 1",
    content: "Détails sur la destination 1",
    startDate: new Date(),
    endDate: new Date(),
  },
  {
    id: 2,
    title: "Destination 2",
    content: "Détails sur la destination 2",
    startDate: new Date(),
    endDate: new Date(),
  },
  {
    id: 3,
    title: "Destination 3",
    content: "Détails sur la destination 3",
    startDate: new Date(),
    endDate: new Date(),
  },
  {
    id: 4,
    title: "Destination 4",
    content: "Détails sur la destination 4",
    startDate: new Date(),
    endDate: new Date(),
  },
  {
    id: 5,
    title: "Destination 5",
    content: "Détails sur la destination 5",
    startDate: new Date(),
    endDate: new Date(),
  },
];

const Itineraire = () => {
  const [sections, setSections] = useState(SECTIONS);
  const [activeSection, setActiveSection] = useState(null);

  const handleToggleSection = (sectionId) => {
    setActiveSection(activeSection === sectionId ? null : sectionId);
  };

  const handleDelete = (sectionId) => {
    const updatedSections = sections.filter(
      (section) => section.id !== sectionId
    );
    setSections(updatedSections);
    if (activeSection === sectionId) {
      setActiveSection(null);
    }
  };

  const handleUpdate = (sectionId, field, value) => {
    const updatedSections = sections.map((section) => {
      if (section.id === sectionId) {
        return { ...section, [field]: value };
      }
      return section;
    });
    setSections(updatedSections);
  };

  const handleDragRelease = (item, bounds) => {
    const newPosition = Math.round(bounds.top / 50);
    const oldPosition = sections.findIndex((section) => section.id === item.id);
    if (newPosition !== oldPosition) {
      const updatedSections = [...sections];
      const [removedSection] = updatedSections.splice(oldPosition, 1);
      updatedSections.splice(newPosition, 0, removedSection);
      setSections(updatedSections);
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>
        <Image
          source={require("../../assets/maps.png")}
          style={styles.backgroundImage}
        />
        {/* <View style={styles.draggableContainer}>
          {sections.map((item, index) => (
            <Draggable
              key={item.id}
              onDragRelease={(event, gestureState, bounds) =>
                handleDragRelease(item, bounds)
              }
              onPressIn={() => handleToggleSection(item.id)}
              x={0}
              y={index * 50}
            >
              <TouchableOpacity
                style={[
                  styles.sectionContainer,
                  activeSection === item.id && styles.activeSectionContainer,
                ]}
              >
                <View style={styles.sectionHeader}>
                  <TouchableOpacity
                    style={styles.titleContainer}
                    onPress={() => handleToggleSection(item.id)}
                  >
                    <Text style={styles.sectionTitle}>{item.title}</Text>
                    <Icon
                      name={
                        activeSection === item.id ? "chevron-up" : "chevron-down"
                      }
                      size={20}
                      color="#555"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => handleDelete(item.id)}>
                    <Icon name="trash" size={20} color="#555" />
                  </TouchableOpacity>
                </View>
                <Collapsible collapsed={activeSection !== item.id}>
                  <View style={styles.sectionContent}>
                    <Text>Date de début:</Text>
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) =>
                        handleUpdate(item.id, "startDate", value)
                      }
                      value={item.startDate.toLocaleDateString()}
                    />
                    <Text>Date de fin:</Text>
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) =>
                        handleUpdate(item.id, "endDate", value)
                      }
                      value={item.endDate.toLocaleDateString()}
                    />
                    <Text>Détails:</Text>
                    <TextInput
                      style={styles.input}
                      onChangeText={(value) =>
                        handleUpdate(item.id, "content", value)
                      }
                      value={item.content}
                    />
                  </View>
                </Collapsible>
              </TouchableOpacity>
            </Draggable>
          ))}
        </View> */}

        <View style={styles.timelineContainer}>
          <Timeline
            style={styles.timeline}
            lineColor="#00008b"
            innerCircleColor="#00008b"
            data={sections.map((section) => ({
              time: section.startDate.toLocaleTimeString(),
              title: section.title,
              description: section.content,
            }))}
          />
        </View>
      </View>
    </ScrollView>
  );
};

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({

  scrollContainer: {
    flexGrow: 1,
  },

  draggableContainer: {
   position: 'relative'
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  timelineContainer: {
    flexGrow: 1,
  },

  activeSectionContainer: {
    marginTop: -16,
  },

  backgroundImage: {
    width: width,
    height: height / 3,
    resizeMode: "contain",
  },

  titleContainer: {
    flexDirection: "row",
    alignItems: "center",
  },

 
  // backgroundImage: {
  //   width: width,
  //   height: height / 3,
  //   resizeMode: 'cover',
  //   position: 'absolute',
  //   top: 0,
  //   left: 0,
  //   zIndex: -1,
  // },
  sectionContainer: {
    flexGrow: 1,
    marginBottom: 16,
    width: width - 32,
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.2,
    shadowRadius: 4,
    elevation: 4,
  },
  sectionHeader: {
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    flexDirection: "row",
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    backgroundColor: "#f7f7f7",
  },
  sectionTitle: {
    flex: 1,
    fontSize: 16,
    fontWeight: "bold",
    color: "#333",
  },
  sectionContent: {
    flexGrow: 0,
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    backgroundColor: "#fff",
  },
  input: {
    height: 40,
    borderColor: "#ccc",
    borderWidth: 1,
    marginBottom: 8,
    paddingHorizontal: 8,
  },
  timeline: {
    paddingBottom: 100,
    flex: 1,
    width: "100%",
    paddingTop: 10,
  },
});

export default Itineraire;
