import { Text, StyleSheet, View, TouchableOpacity } from 'react-native'
import React from 'react'
import AccountCard from '../components/AccountCard'

export default function AccountScreen({ user, itineraries, history }){
    
      return (
        <View style={styles.container}>
          <AccountCard />
        </View>
      );
    }
    
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f9f9f9',
    alignItems: 'center',
    paddingTop: 20,
  },
});
