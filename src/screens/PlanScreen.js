import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, SafeAreaView, ScrollView, TextInput, Button } from 'react-native';
// import { Calendar } from 'expo-calendar';
// import DateTimePickerModal from "react-native-modal-datetime-picker";

export default function PlanScreen() {
  const [selectedDate, setSelectedDate] = useState(null);
  const [companionName, setCompanionName] = useState("");
  const [cartItems, setCartItems] = useState([]);

  // const showCalendar = async () => {
  //   const { status } = await Calendar.requestCalendarPermissionsAsync();
  //   if (status === 'granted') {
  //     const calendar = await Calendar.getCalendarsAsync();
  //     const selectedDate = await Calendar.launchCalendarAsync();
  //     setSelectedDate(selectedDate);
  //   } else {
  //     console.log('Permission not granted');
  //   }
  // };

  const addToCart = () => {
    if (companionName) {
      const newItem = {
        date: selectedDate ? selectedDate.toLocaleDateString() : "Pas de date sélectionnée",
        companion: companionName
      };
      setCartItems([...cartItems, newItem]);
      setSelectedDate(null);
      setCompanionName("");
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Choisissez une date de départ</Text>
          <TouchableOpacity style={styles.button} onPress={''}>
            <Text style={styles.buttonText}>{selectedDate ? selectedDate.toLocaleDateString() : "Sélectionner une date"}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Ajouter un compagnon de voyage</Text>
          <TextInput
            style={styles.input}
            placeholder="Nom du compagnon"
            value={companionName}
            onChangeText={setCompanionName}
          />
        </View>
        <View style={styles.section}>
          <Button title="Ajouter au panier" onPress={addToCart} />
        </View>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Panier</Text>
          {cartItems.map((item, index) => (
            <View key={index} style={styles.cartItem}>
              <Text>{item.date} - {item.companion}</Text>
            </View>
          ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e7e3f1',
  },
  scrollView: {
    padding: 16,
  },
  section: {
    marginBottom: 16,
    borderRadius: 50
  },
  sectionTitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  button: {
    backgroundColor: '#eee',
    padding: 16,
    borderRadius: 8,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  input: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    padding: 8,
    marginTop: 8,
  },
  cartItem: {
    backgroundColor: '#eee',
    padding: 16,
    borderRadius: 8,
    marginBottom: 8,
  },
});
