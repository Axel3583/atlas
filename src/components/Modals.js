import { Ionicons } from "@expo/vector-icons";
import React, { useState } from "react";
import {
  Modal,
  Text,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Pressable,
} from "react-native";
import { SimpleLineIcons } from "@expo/vector-icons";

export default function ImageModal({
  item,
  modalVisible,
  onClose,
  onAddToCart,
  onRemoveFromCart,
}) {
  const [showDeleteButton, setShowDeleteButton] = useState(false);
  const [showPressable, setShowPressable] = useState(true);

  const handlePress = () => {
    console.log("Ajouter");
    setShowPressable(false);
    setShowDeleteButton(true);
    onAddToCart(item);
    
  };
  const handleDelete = () => {
    setShowPressable(true);
    setShowDeleteButton(false);
    console.log("Deleted");
    onRemoveFromCart(item.id);
  };

  if (!item) {
    return null; // return null if item is null or undefined
  }

  return (
    <Modal animationType="fade" visible={modalVisible} onRequestClose={onClose}>
      {/* <View style={styles.modalContainer}> */}
      <View style={styles.placeContainer}>
        <View style={styles.cityImg}>
          <Image source={{ uri: item.cityImage }} style={styles.img} />
        </View>
        <TouchableOpacity style={styles.placeNameContainer}>
          <Ionicons
            onPress={onClose}
            name="chevron-back"
            size={24}
            color="#841584"
          />
        </TouchableOpacity>
      </View>
      <View style={styles.regionNameStyle}>
        <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 10 }}>
          {item.name}
        </Text>
        <TouchableOpacity>
          <Ionicons name="heart-dislike-outline" size={30} color="pink" />
        </TouchableOpacity>
      </View>
      <View style={styles.cityNameStyle}>
        <SimpleLineIcons name="location-pin" size={20} color="pink" />
        <Text style={{ marginLeft: 5 }}>{item.name}</Text>
      </View>
      <ScrollView showsHorizontalScrollIndicator={false}>
        <View style={styles.textContainer}>
          <Text>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo velit
            reiciendis repudiandae recusandae id optio, et eligendi officiis,
            labore quisquam nesciunt cupiditate. Dolore numquam iste quae, non
            perferendis reprehenderit similique! Lorem ipsum dolor sit amet,
            consectetur adipisicing elit. Quo velit reiciendis repudiandae
            recusandae id optio, et eligendi officiis, labore quisquam nesciunt
            cupiditate. Dolore numquam iste quae, non perferendis reprehenderit
            similique! Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quo velit reiciendis repudiandae recusandae id optio, et eligendi
            officiis, labore quisquam nesciunt cupiditate. Dolore numquam iste
            quae, non perferendis reprehenderit similique!
          </Text>
        </View>
      </ScrollView>
      <View style={styles.pressContainer}>
        {showPressable && (
          <Pressable style={styles.pressable} onPress={handlePress}>
            <Text>Ajouter au parcours</Text>
          </Pressable>
        )}
        {showDeleteButton && (
          <Pressable style={styles.deleteBtn} onPress={handleDelete}>
            <Text>Retirer du parcours</Text>
          </Pressable>
        )}
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    width: "100%",
    padding: 20,
    backgroundColor: "#fff",
    borderRadius: 10,
  },
  modalTitle: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 10,
  },
  modalDescription: {
    fontSize: 16,
    lineHeight: 24,
  },

  img: {
    width: "100%",
    height: 350,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  regionNameStyle: {
    justifyContent: "space-between",
    flexDirection: "row",
    paddingTop: 30,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  cityNameStyle: {
    paddingHorizontal: 20,
    flexDirection: "row",
  },
  textContainer: {
    paddingHorizontal: 20,
    paddingTop: 15,
  },
  pressable: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#841584",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingVertical: 10,
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  placeContainer: {
    position: "relative",
  },
  placeNameContainer: {
    position: "absolute",
    paddingTop: 5,
    left: 10,
    marginTop: 5,
  },
  deleteBtn: {
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    paddingVertical: 10,
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
  pressContainer: {
    shadowColor: "#000",
    shadowOffset: { width: 2, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
  },
});
