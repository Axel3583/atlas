import React, { useState } from "react";
import {
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  Button,
} from "react-native";
import Slider from "@react-native-community/slider";
import DateTimePicker from "@react-native-community/datetimepicker";

export default function InformationUserScreen({ handleDoneInterest }) {
  const [address, setAddress] = useState("");
  const [departureDate, setDepartureDate] = useState("");
  const [budget, setBudget] = useState(550);
  const [numParticipants, setNumParticipants] = useState(1);
  const [preferences, setPreferences] = useState([]);
  const [showDatePicker, setShowDatePicker] = useState(false);
  const [showPreferences, setShowPreferences] = useState(false);

  return (
    <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
      <View>
        <Image source={require("../../assets/map.jpg")} style={styles.image} />
      </View>
      <View style={styles.inputContainer}>
        <Text style={styles.label}>Adresse</Text>
        <TextInput
          style={styles.input}
          onChangeText={setAddress}
          value={address}
          placeholder="Entrez votre adresse"
        />
        {showDatePicker && (
          <DateTimePicker
            value={departureDate}
            mode={"date"}
            display="default"
            onChange={(event, selectedDate) => {
              setShowDatePicker(false);
              setDepartureDate(selectedDate || departureDate);
            }}
          />
        )}
        <TouchableOpacity
          style={styles.button}
          onPress={() => setShowDatePicker(true)}
        >
          <Text style={styles.buttonText}>Date de depart</Text>
        </TouchableOpacity>
        <Text style={styles.label}>Budget</Text>
        <Slider
          style={styles.slider}
          minimumValue={100}
          maximumValue={1000}
          step={50}
          value={budget}
          onValueChange={setBudget}
          minimumTrackTintColor="#8b0000"
          maximumTrackTintColor="red"
        />
        <Text style={styles.sliderValue}>{budget}$</Text>
        <Text style={styles.label}>Nombre de participants</Text>
        <View style={styles.participantsContainer}>
          <TouchableOpacity
            style={styles.buttonPlus}
            onPress={() => setNumParticipants(numParticipants + 1)}
          >
            <Text style={styles.buttonText}>+</Text>
          </TouchableOpacity>
          <Text>{numParticipants}</Text>
          <TouchableOpacity
            style={styles.buttonPlus}
            onPress={() => setNumParticipants(Math.max(1, numParticipants - 1))}
          >
            <Text style={styles.buttonText}>-</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.label}>Préférences</Text>
        <TextInput
          style={styles.input}
          onChangeText={setPreferences}
          value={preferences}
          placeholder="Entrez vos préférences"
        />
        <TouchableOpacity style={styles.button} onPress={handleDoneInterest}>
          <Text style={styles.buttonText}>Valider</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
}

const { width, height } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f5f5f5",
  },

  participantsContainer: {
    alignItems: "center",
  },
  sliderValue: {
    fontFamily: "poppins",
  },
  inputContainer: {
    margin: 20,
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 5,
  },

  input: {
    height: 40,
    fontFamily: "poppins",
    borderColor: "#ddd",
    borderBottomWidth: 1,
    marginBottom: 20,
  },

  label: {
    fontSize: 15,
    fontFamily: "poppins",
    marginBottom: 5,
    color: "#333",
    paddingVertical: 12,
  },

  buttonPlus: {
    backgroundColor: "#00008b",
    fontFamily: "poppins",
    padding: 10,
    borderRadius: 30,
    alignItems: "center",
    paddingVertical: 5,
    width: 50,
  },
  button: {
    backgroundColor: "#00008b",
    fontFamily: "poppins",
    padding: 10,
    borderRadius: 30,
    alignItems: "center",
    paddingVertical: 5,
  },

  buttonText: {
    color: "#fff",
    fontFamily: "poppins",
    fontSize: 13,
  },

  image: {
    width: width,
    height: 250,
    borderBottomRightRadius: 25,
    borderBottomLeftRadius: 25,
    backgroundColor: "#f5f5f5",
    shadowColor: "#f0edf6",
  },
});
