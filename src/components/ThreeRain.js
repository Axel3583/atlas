// import React, { useRef } from 'react';
// import { Text, StyleSheet, View, Dimensions } from 'react-native';
// import { GLView } from 'expo-gl';
// import { Scene, PerspectiveCamera, SphereGeometry, MeshBasicMaterial, Mesh, TextureLoader, RepeatWrapping } from 'three';
// import { Renderer } from 'expo-three';

// export default function ThreeEarth({ children }) {
//   const renderer = useRef(null);

//   const onContextCreate = async (gl) => {
//     const scene = new Scene();
//     const camera = new PerspectiveCamera(75, gl.drawingBufferWidth / gl.drawingBufferHeight, 0.1, 1000);
//     camera.position.z = 2;

//     // create sphere geometry for the earth
//     const sphereGeometry = new SphereGeometry(1, 32, 32);
//     const texture = new TextureLoader().load('https://image.spreadshirtmedia.net/image-server/v1/mp/products/T1459A839PA4459PT28D181123407W10000H10000/views/1,width=1200,height=630,appearanceId=839,backgroundColor=F2F2F2/globe-terrestre-monde-planete-terre-globe-autocollant.jpg');
//     texture.wrapS = THREE.RepeatWrapping;
//     texture.wrapT = THREE.RepeatWrapping;
//     texture.repeat.set(1, 1);
    
    
//     // create material for the earth
//     const material = new MeshBasicMaterial({
//       map: texture,
//     });
//     // const sphere = new THREE.Mesh(new THREE.BufferGeometry(1, 32, 32), material);
//     // material.side = THREE.BackSide;

//     // create mesh object and add to the scene
//     const earth = new Mesh(sphereGeometry, material);
//     scene.add(earth);

//     renderer.current = new Renderer({ gl });

//     const render = () => {
//       earth.rotation.x += 0.030;
//       earth.rotation.y += 0.020;
//       renderer.current.render(scene, camera);
//       gl.endFrameEXP();
//       requestAnimationFrame(render);
//     };

//     render();
//   };

//   return (
//     <View style={styles.container}>
//       <GLView style={styles.glView} onContextCreate={onContextCreate}>
//       { children }
//       </GLView>
//     </View>
//   );
// }

// const { width, height } = Dimensions.get('window')

// const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//       alignItems: 'center',
//       justifyContent: 'center',
//       backgroundColor: '#f5f5f5',
//       shadowColor: 'black',
//       shadowOpacity: 10,
//       width: width,
//       height: height,
//     },
//     title: {
//       fontSize: 24,
//       fontWeight: 'bold',
//       color: 'white',
//       marginBottom: 20,
//     },
//     glView: {
//       aspectRatio: 1,
//       width: width,
//       height: height,
//       maxWidth: '100%',
//       maxHeight: '100%',
//     },
//   });
  
