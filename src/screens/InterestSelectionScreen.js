import React, { useState } from 'react';
import { 
  View, 
  Text, 
  FlatList, 
  TouchableOpacity, 
  StyleSheet, 
  Animated 
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const interests = [
    { name: 'Hiking', icon: 'hiking' },
    { name: 'Beach', icon: 'beach' },
    { name: 'City Tours', icon: 'city' },
    // { name: 'Museums', icon: 'museum' },
    // { name: 'Theater', icon: 'theater' },
    { name: 'Music Festivals', icon: 'music' },
    // { name: 'Art', icon: 'art' },
    // { name: 'History', icon: 'history' },
    // { name: 'Technology', icon: 'tech' },
    // { name: 'Sports', icon: 'sports-soccer' },
    { name: 'Food & Drinks', icon: 'food' },
    // { name: 'Shopping', icon: 'shopping' },
    // { name: 'Wildlife', icon: 'wildlife' },
    // { name: 'Nightlife', icon: 'nightlife' },
    // { name: 'Boating', icon: 'boat' },
    // { name: 'Fishing', icon: 'fishing' },
    { name: 'Cycling', icon: 'bike' },
    // { name: 'Surfing', icon: 'surf' },
    { name: 'Yoga', icon: 'meditation' },
    // { name: 'Dance', icon: 'dance' },
    { name: 'Sports', icon: 'soccer' },
    { name: 'Musées', icon: 'bank' },
    { name: 'Shopping', icon: 'shopping' },
    { name: 'Golf', icon: 'golf' },
    // { name: 'Bird Watching', icon: 'bird' },
    // { name: 'Monuments Historiques', icon: 'town-hall' },
    // { name: 'Histoire et Patrimoine', icon: 'history' },
    // { name: 'Spa & Wellness', icon: 'spa' },
    // { name: 'Rock Climbing', icon: 'climbing' },
    // { name: 'Skydiving', icon: 'skydiving' },
    // { name: 'Fitness', icon: 'fitness' },
    // { name: 'Cooking', icon: 'cooking' },
    // { name: 'Wine Tasting', icon: 'wine' },
    // { name: 'Reading', icon: 'reading' },
    { name: 'Châteaux', icon: 'castle' },
    { name: 'Vins et Gastronomie', icon: 'bottle-wine' },
    { name: 'Montagnes et Randon', icon: 'image-filter-hdr' },
    // { name: 'Art et Culture', icon: 'palette' },
    { name: 'Nature et Parcs', icon: 'nature' },
    // { name: 'Rivières et Canoë', icon: 'beach' },
    { name: 'Photographie', icon: 'camera' },
    { name: 'Plages et Mer', icon: 'beach' },
    { name: 'Cyclisme', icon: 'bike' },
    { name: 'Bien-être et Spa', icon: 'spa' },
    { name: 'Cuisine et Ateliers', icon: 'chef-hat' },
    { name: 'Visites Guidées', icon: 'dog-service' },
    { name: 'Activités pour Enfants', icon: 'teddy-bear' },
  ];
  
//   const interests = [
//     { name: 'Châteaux', icon: 'castle' },
//     { name: 'Musées', icon: 'museum' },
//     { name: 'Vins et Gastronomie', icon: 'wine' },
//     { name: 'Histoire et Patrimoine', icon: 'history' },
//     { name: 'Montagnes et Randonnées', icon: 'mountains' },
//     { name: 'Plages et Mer', icon: 'beach' },
//     { name: 'Art et Culture', icon: 'art' },
//     { name: 'Shopping et Mode', icon: 'shopping' },
//     { name: 'Monuments Historiques', icon: 'monument' },
//     { name: 'Festivals et Spectacles', icon: 'festival' },
//     { name: 'Nature et Parcs', icon: 'nature' },
//     { name: 'Rivières et Canoë', icon: 'canoe' },
//     { name: 'Photographie', icon: 'camera' },
//     { name: 'Sports', icon: 'sports' },
//     { name: 'Cyclisme', icon: 'cycling' },
//     { name: 'Golf', icon: 'golf' },
//     { name: 'Bien-être et Spa', icon: 'spa' },
//     { name: 'Cuisine et Ateliers', icon: 'cooking' },
//     { name: 'Visites Guidées', icon: 'guided-tours' },
//     { name: 'Activités pour Enfants', icon: 'kids' },
//   ];
  
const InterestItem = ({ item, isSelected, onSelect }) => {
    const scaleValue = useState(new Animated.Value(1))[0];
  
    const itemPressed = () => {
      Animated.sequence([
        Animated.timing(scaleValue, {
          toValue: 0.9,
          duration: 100,
          useNativeDriver: true,
        }),
        Animated.timing(scaleValue, {
          toValue: 1,
          duration: 100,
          useNativeDriver: true,
        }),
      ]).start();
  
      onSelect(item);
    };
  
    return (
      <Animated.View style={[styles.item, isSelected ? styles.selectedItem : {}, { transform: [{ scale: scaleValue }] }]}>
        <TouchableOpacity activeOpacity={0.8} onPress={itemPressed}>
          <View style={styles.itemContent}>
            <MaterialCommunityIcons name={item.icon} size={24} color={isSelected ? 'blue' : 'black'} />
            <Text style={styles.title}>{item.name}</Text>
          </View>
        </TouchableOpacity>
      </Animated.View>
    );
  }
  
  export default function InterestSelectionScreen({ handleDone }) {
    const [selectedIds, setSelectedIds] = useState([]);
  
    const onSelect = (item) => {
      if (selectedIds.includes(item.name)) {
        setSelectedIds(selectedIds.filter(id => id !== item.name));
      } else {
        setSelectedIds([...selectedIds, item.name]);
      }
    }
  
    const renderItem = ({ item }) => {
      return (
        <InterestItem 
          item={item}
          isSelected={selectedIds.includes(item.name)}
          onSelect={onSelect}
        />
      );
    };
  
    return (
      <View style={styles.container}>
        <Text style={styles.header}>Choisissez vos centres d'intérêt</Text>
        <FlatList showsVerticalScrollIndicator={false}
          data={interests}
          renderItem={renderItem}
          keyExtractor={(item) => item.name}
          extraData={selectedIds}
          numColumns={3}
        />
        <TouchableOpacity style={styles.button} onPress={handleDone}>
          <Text style={styles.buttonText}>Continuer</Text>
        </TouchableOpacity>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#dcdcdc',
      paddingTop: 10
    },
    header: {
      fontSize: 18,
      fontWeight: 'normal',
      textAlign: 'center',
      marginVertical: 20,
    },
    item: {
      backgroundColor: '#fff',
      padding: 5,
      marginVertical: 8,
      marginHorizontal: 5,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 5,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.3,
      shadowRadius: 3,
      elevation: 3,
    },
    selectedItem: {
      backgroundColor: 'skyblue',
    },
    itemContent: {
      alignItems: 'center',
    },
    title: {
      fontSize: 16,
    },
    button: {
      backgroundColor: "rgba(91, 91, 102, 1)",
      padding: 10,
      alignItems: 'center',
      borderRadius: 40,
      marginVertical: 20,
     
    },
    buttonText: {
      color: 'white',
      fontSize: 18,
    },
  });