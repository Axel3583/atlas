import { StyleSheet, View } from 'react-native'
import React from 'react'
import { Triangle } from 'react-loader-spinner'

export default function Spinner() {

    return (
        <View style={styles.container}>
            <Triangle
                height="80"
                width="80"
                color="#191970"
                ariaLabel="triangle-loading"
                wrapperStyle={{}}
                wrapperClassName=""
                visible={true}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }
})