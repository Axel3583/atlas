import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";

const Button = ({ onPress, text, iconName }) => (
  <TouchableOpacity style={styles.button} onPress={onPress}>
    <MaterialIcons name={iconName} size={24} color="#00008b" />
    <Text style={styles.buttonText}>{text}</Text>
    <MaterialIcons name="keyboard-arrow-right" size={24} color="#00008b" />
  </TouchableOpacity>
);

export default function AccountCard({
  title,
  value,
  onPersonalInfoPress,
  onPlannedRoutesPress,
  onRouteHistoryPress,
}) {
  return (
    <LinearGradient colors={["#f5f5f5", "#d3d3d3", "#f5f5f5"]} style={{ flex: 1 }}>
      <View style={styles.container}>
        <View style={styles.profile}>
          <View style={styles.imageContainer}>
            <Image
              source={{ uri: "https://randomuser.me/api/portraits/men/32.jpg" }}
              style={styles.image}
            />
          </View>
          <View style={styles.userInfo}>
            <Text style={styles.username}>John Doe</Text>
            <Text style={styles.email}>johndoe@gmail.com</Text>
          </View>
        </View>
        <View style={styles.buttonsContainer}>
          <Button
            onPress={onPersonalInfoPress}
            text="Modifier mes informations"
            iconName="edit"
          />
          <Button
            onPress={onPlannedRoutesPress}
            text="Consulter mon itinéraire"
            iconName="directions"
          />
          <Button
            onPress={onRouteHistoryPress}
            text="Consulter mon historique"
            iconName="history"
          />
        </View>
      </View>
    </LinearGradient>
  );
}

const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    padding: 20,
    shadowOpacity: 0.25,
    elevation: 5,
    width: width,
  },
  profile: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 20,
  },
  imageContainer: {
    width: 70,
    height: 70,
    borderRadius: 35,
    marginRight: 20,
    overflow: "hidden",
  },
  image: {
    width: "100%",
    height: "100%",
  },
  userInfo: {
    flex: 1,
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 5,
    fontFamily: "Poppins",
  },
  email: {
    color: "#666",
    fontFamily: "Poppins",
  },
  buttonsContainer: {
    flexDirection: "column",
    justifyContent: "space-between",
    color: "#fff",
  },
  button: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#007AFF",
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 10,
    justifyContent: "space-between",
    color: "#fff",
    backgroundColor: "#fff",
    shadowColor: "#f0edf6",
    shadowOffset: { width: 1, height: 2 },
    shadowOpacity: 5,
    shadowRadius: 1,
  },
  buttonText: {
    color: "#000",
    marginLeft: 10,
    fontWeight: "bold",
    width: 250,
    fontFamily: "Poppins",
  },
});
