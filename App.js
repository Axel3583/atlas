import React, { useState, useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import * as Font from "expo-font";
import { MaterialCommunityIcons, AntDesign } from "@expo/vector-icons";
import { Fontisto } from "@expo/vector-icons";

import HomeScreen from "./src/screens/HomeScreen";
import LoginScreen from "./src/screens/LoginScreen";
import MapScreen from "./src/screens/MapScreen";
import PlanScreen from "./src/screens/PlanScreen";
import Root from "./src/components/Root";
import Spinner from "./src/components/Spinner";
import FranceMap from "./src/screens/Itineraire";
import OnboardingScreen from "./src/screens/OnboardingScreen";
import InformationUserScreen from "./src/screens/InformationUserScreen";

const Tab = createMaterialBottomTabNavigator();

const tabs = [
  {
    name: "Map",
    component: MapScreen,
    icon: "map-outline",
    iconLib: MaterialCommunityIcons,
  },
  // { name: "Login", component: LoginScreen, icon: "login", iconLib: AntDesign, color: "black", size: 20 },
  { name: "Home", component: HomeScreen, icon: "search1", iconLib: AntDesign },
  {
    name: "Plan",
    component: PlanScreen,
    icon: "calendar-outline",
    iconLib: MaterialCommunityIcons,
  },
  { name: "Root", component: Root, icon: "atlassian", iconLib: Fontisto },
];

export default function App() {
  const [loader, setLoader] = useState(true);
  const [isFirstLaunch, setIsFirstLaunch] = React.useState(false);
  const [interestSelectionDone, setInterestSelectionDone] = useState(false);
  const [informationSelectionDone, setInformationSelectionDone] =
    useState(false);

  const loadResources = async () => {
    try {
      await Font.loadAsync({
        poppins: require("./assets/9/fonts/Poppins-Regular.ttf"),
        LeckerliOne: require("./assets/9/fonts/LeckerliOne-Regular.ttf"),
      });
      setTimeout(() => setLoader(false), 1000);
    } catch (error) {
      console.error("ressources indisponible :", error);
      // Considérez un gestionnaire d'erreurs plus robuste ici
    }
  };

  const handleDone = () => {
    setIsFirstLaunch(true);
    // setInformationSelectionDone(true)
    console.log("done");
  };

  const handleDoneInterest = () => {
    setInformationSelectionDone(true);
    console.log("done");
  };

  useEffect(() => {
    loadResources();
  }, []);

  return (
    <View style={styles.center}>
      {loader ? (
        <Spinner />
      ) : !isFirstLaunch ? (
        <OnboardingScreen handleDone={handleDone} />
      ) : !informationSelectionDone ? (
        <InformationUserScreen handleDoneInterest={handleDoneInterest} />
      ) : (
        <NavigationContainer>
          <Tab.Navigator
            initialRouteName="Home"
            activeColor="#000080"
            inactiveColor="#000080"
            barStyle={{ shadowColor: "#f0edf6", height: 50, borderRadius: 50 }}
          >
            {tabs.map(
              ({
                name,
                component,
                icon,
                iconLib,
                color = "#000080",
                size = 26,
              }) => {
                const IconComponent = iconLib;
                return (
                  <Tab.Screen
                    key={name}
                    name={name}
                    component={component}
                    options={{
                      tabBarLabel: name === "Login" ? "Login" : "",
                      tabBarIcon: ({ color }) => (
                        <IconComponent name={icon} size={size} color={color} />
                      ),
                    }}
                  />
                );
              }
            )}
          </Tab.Navigator>
        </NavigationContainer>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
  },
});
