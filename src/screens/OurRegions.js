import React, { useState } from "react";
import { Dimensions } from "react-native"
import { useNavigation } from '@react-navigation/native';
import {
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import Title from "../components/Title";
import SearchBar from "../components/SearchBar";


const regionsData = [
  {
    id: '1',
    regionName: 'Auvergne-Rhône-Alpes',
    cityName: 'Lyon',
    imageUri: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg',
    culturalPlaces: [
      { id: '1', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '2', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '3', name: 'Vieux Lyon', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '4', name: 'Musée des Beaux-Arts de Lyon', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '5', name: 'Basilique Notre-Dame de Fourvière', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
    ],
    restaurants: [
      { id: '1', name: 'L\'Atelier des Augustins', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '2', name: 'Les Apothicaires', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '3', name: 'La Mère Brazier', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '4', name: 'Les Apothicaires', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' },
      { id: '5', name: 'La Mère Brazier', cityImage: 'https://www.selexium.com/app/uploads/2021/07/Le-Havre.jpg' }
    ]
  },
  {
    id: '2',
    regionName: 'Bourgogne-Franche-Comté',
    cityName: 'Dijon',
    imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg',
    culturalPlaces: [
      { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
      { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
      { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
    ],
    restaurants: [
      { id: '1', name: 'L\'Essentiel', imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
      { id: '2', name: 'Le Pré aux Clercs' },
      { id: '3', name: 'La Maison des Cariatides' }
    ]
  },
  {
    id: '3',
    regionName: 'Bretagne',
    cityName: 'Rennes',
    imageUri: 'https://www.lehavreseinemetropole.fr/sites/default/files/media/images/citenumerique-lehavre-enseignementsuperieur-innovation.jpg',
    culturalPlaces: [
      {
        "id": "1",
        "name": "Musée des Beaux-Arts de Dijon"
      },
      {
        "id": "2",
        "name": "Palais des Ducs et des États de Bourgogne"
      },
      {
        "id": "3",
        "name": "Cathédrale Saint-Bénigne de Dijon"
      }
    ],
    restaurants: [
      {
        "id": "1",
        "name": "L'Essentiel"
      },
      {
        "id": "2",
        "name": "Le Pré aux Clercs"
      },
      {
        "id": "3",
        "name": "La Maison des Cariatides"
      }
    ]
  },
  {
    id: '4',
    regionName: 'Centre-Val de Loire',
    cityName: 'Orléans',
    imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/5_monuments_historiques_a___visiter_en_france.jpg',
    culturalPlaces: [
      {
        "id": "1",
        "name": "Musée des Beaux-Arts de Dijon"
      },
      {
        "id": "2",
        "name": "Palais des Ducs et des États de Bourgogne"
      },
      {
        "id": "3",
        "name": "Cathédrale Saint-Bénigne de Dijon"
      }
    ],
    restaurants: [
      {
        "id": "1",
        "name": "L'Essentiel"
      },
      {
        "id": "2",
        "name": "Le Pré aux Clercs"
      },
      {
        "id": "3",
        "name": "La Maison des Cariatides"
      }
    ]
  },
  {
    id: '5',
    regionName: 'Corse',
    cityName: 'Ajaccio',
    imageUri: 'https://keewego.com/wp-content/uploads/2018/10/Arc-de-triomphe-11-768x512.jpg',
    culturalPlaces: [
      {
        "id": "1",
        "name": "Musée des Beaux-Arts de Dijon"
      },
      {
        "id": "2",
        "name": "Palais des Ducs et des États de Bourgogne"
      },
      {
        "id": "3",
        "name": "Cathédrale Saint-Bénigne de Dijon"
      }
    ],
    restaurants: [
      {
        "id": "1",
        "name": "L'Essentiel"
      },
      {
        "id": "2",
        "name": "Le Pré aux Clercs"
      },
      {
        "id": "3",
        "name": "La Maison des Cariatides"
      }
    ]
  },
  {
    id: '6',
    regionName: 'Grand Est',
    cityName: 'Strasbourg',
    imageUri: 'https://foulecity.com/wp-content/uploads/2018/01/visite-virtuelle-monument-historique.jpg',
    culturalPlaces: [
      {
        "id": "1",
        "name": "Musée des Beaux-Arts de Dijon"
      },
      {
        "id": "2",
        "name": "Palais des Ducs et des États de Bourgogne"
      },
      {
        "id": "3",
        "name": "Cathédrale Saint-Bénigne de Dijon"
      }
    ],
    restaurants: [
      {
        "id": "1",
        "name": "L'Essentiel"
      },
      {
        "id": "2",
        "name": "Le Pré aux Clercs"
      },
      {
        "id": "3",
        "name": "La Maison des Cariatides"
      }
    ]
  },
  {
    id: '7',
    regionName: 'Hauts-de-France',
    cityName: 'Lille',
    imageUri: 'https://picsum.photos/id/243/200/300',
    culturalPlaces: [
      { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
      { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
      { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
    ],
    restaurants: [
      { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
      { id: '2', name: 'Le Pré aux Clercs' },
      { id: '3', name: 'La Maison des Cariatides' }
    ]
  },
  {
    id: '8',
    regionName: 'Île-de-France',
    cityName: 'Paris',
    imageUri: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-1.jpg',
    culturalPlaces: [
      { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
      { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
      { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
    ],
    restaurants: [
      { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
      { id: '2', name: 'Le Pré aux Clercs' },
      { id: '3', name: 'La Maison des Cariatides' }
    ]
  },
  {
    id: '9',
    regionName: 'Normandie',
    cityName: 'Rouen',
    imageUri: 'https://a.cdn-hotels.com/gdcs/production139/d1145/9d3a4af5-34c1-4bb4-baa3-6096e7291ebd.jpg',
    culturalPlaces: [
      { id: '1', name: 'Musée des Beaux-Arts de Dijon' },
      { id: '2', name: 'Palais des Ducs et des États de Bourgogne' },
      { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon' }
    ],
    restaurants: [
      { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
      { id: '2', name: 'Le Pré aux Clercs', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg'  },
      { id: '3', name: 'La Maison des Cariatides', cityImage:'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
    ]
  },
  {
    id: '10',
    regionName: 'Nouvelle-Aquitaine',
    cityName: 'Bordeaux',
    imageUri: 'https://images.france.fr/zeaejvyq9bhj/2AIPIRpD49bBDj5351aSgi/37993a373b371a80c0ee563548cf870f/ok-Pierrefonds_nord-est__Christian_Gluckman_CMN.JPG?w=1120&h=490&q=70&fl=progressive&fit=fill',
    culturalPlaces: [
      { id: '1', name: 'Musée des Beaux-Arts de Dijon', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg'  },
      { id: '2', name: 'Palais des Ducs et des États de Bourgogne', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg'  },
      { id: '3', name: 'Cathédrale Saint-Bénigne de Dijon', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg'  }
    ],
    restaurants: [
      { id: '1', name: 'L\'Essentiel', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg' },
      { id: '2', name: 'Le Pré aux Clercs', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg'  },
      { id: '3', name: 'La Maison des Cariatides', cityImage: 'https://lavaliseafleurs.com/wp-content/uploads/2019/02/Paris-2.jpg'  }
    ]
  },
];


export default function OurRegions() {
  const navigation = useNavigation();

  const Item = ({ item, onPress, isSelected }) => (
    <TouchableOpacity onPress={onPress} style={[styles.item, styles.regionItem, isSelected && styles.selectedItem]}>
      <Image
        source={{ uri: item.imageUri }}
        style={{ width: '100%', height: '75%', borderRadius: 10 }}
      />
      <View style={{ padding: 10 }}>
        <Text style={styles.regionName}>{item.regionName}</Text>
        <Text style={styles.cityName}>{item.cityName}</Text>
      </View>
    </TouchableOpacity>
  );

  const [selectedId, setSelectedId] = useState(null);
  const renderItem = ({ item }) => {
    const isSelected = item.id === selectedId;
    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id);
          navigation.navigate("Explore", { region: item });
        }}
        isSelected={isSelected}
      />
    );
  };



  return (
    <View style={styles.container}>
       <View>
           <Title size={'big'} content={'Nos départements'}/>
        <View>
           <Title size={'medium'} content={'Auvergne-Rhône-Alpes'}/> 
        </View>
       </View>
       <SearchBar />
      <FlatList
        data={regionsData}
        renderItem={(props) =>
          renderItem({ ...props, onPress: setSelectedId, selectedId })
        }
        keyExtractor={(item) => item.id}
        extraData={selectedId}
        numColumns={2}
        // horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.contentContainerStyle}
      />
    </View>
  );
}

const { width, height } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 10
  },
  contentContainerStyle: {
    paddingHorizontal: 16,
  },

//   ourR: {
//     backgroundColor: '#0edf6',
//     shadowColor: '#000',
//     shadowOffset: { width: 1, height: 2 },
//     shadowOpacity: 0.5,
//     shadowRadius: 1,
//     elevation: 2
//   },
  item: {
    width: width / 2.1,
    flex: 1,
    height: height / 2.5,
    borderRadius: 22,
    padding: 12,
    marginRight: 16,
    marginBottom: 16,
    backgroundColor: '#f0edf6',
    shadowColor: '#000',
    // shadowOffset: { width: 0, height: 1 },
    // shadowOpacity: 0.2,
    // shadowRadius: 1,
    elevation: 2
  },
  selectedItem: {
    backgroundColor: '#f0edf6',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  selectedTitle: {
    color: '#fff',
  },
  city: {
    fontSize: 18,
    marginTop: 10,
    paddingHorizontal: 5
  },
  cityName: {
    fontSize: 'small',
    color: 'gray',
    paddingTop: 5,
    paddingHorizontal: 5
  }
});
